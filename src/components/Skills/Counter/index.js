import React, {useState, useEffect, useRef} from 'react';

function Counter({max, step}) {
    const [count, setCount] = useState(0);
    
    useInterval(() => {
      // Your custom logic here
      if (count !== max) {
        setCount(count + 1);
      }
      
    }, step);
    return <p className="contain__skill__text">{count}%</p>;
  }
  
  function useInterval(callback, delay) {
    const savedCallback = useRef();
  
    // Remember the latest function.
    useEffect(() => {
      savedCallback.current = callback;
    }, [callback]);
  
    // Set up the interval.
    useEffect(() => {
      function tick() {
        savedCallback.current();
      }
      if (delay !== null) {
        let id = setInterval(tick, delay);
        return () => clearInterval(id);
      }
    }, [delay]);
  }

  export default Counter;