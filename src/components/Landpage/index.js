import React, {Component} from 'react';
import imgYoan from '../../images/yoan.jpg';

import './Landpage.css';

class Landpage extends Component {
  render() {
    return(
      <div className="landpage" id="accueil">
        
        <p className="landpage__text__title1"> DÉVELOPPEUR D'APPLICATION</p>
        <div className="container__img">
          <img src={imgYoan} alt="yoan_photo" className="landpage__img" />
        </div>
        <div className="container__text">
        </div>
      </div>
    );
  }
}



export default Landpage;
