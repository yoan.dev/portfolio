import React, {Component} from 'react';
import Button from '../../Button';

import './ContainerButton.css';

class ContainerButton extends Component {
  render() {
    const {onClick} = this.props;
    return(
      <div className="container__button">
        {["accueil","présentation","compétences","projets", "contact"].map(item => {
          const handleOnClick = item === 'projets' ? onClick : function(){};
          return <Button name={item} key={item} onClick={handleOnClick}  />
        }
          
        )}
      </div>
    );
  }
}

export default ContainerButton;
