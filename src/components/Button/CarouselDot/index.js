import React, {Component} from 'react';
import {Link} from 'react-scroll';
import './CarouselDot.css';

class CarouselDot extends Component {
  render() {
      const {onClick, value, focus} = this.props;
      const classNameFocus = focus === value ? 'carousel__dot--focus' : 'carousel__dot';
    return(
        <Link
        activeClass="active"
        to='projets'
        spy={true}
        smooth={true}
        offset={0}
        duration= {500}
      >
        <div onClick={(event) => onClick(event, value)} className={classNameFocus} >
        </div>
      </Link>
    );
  }
}

export default CarouselDot;
