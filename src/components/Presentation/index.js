import React, {Component} from 'react';
import TextAbout from './TextAbout';
import ImageLogo from './ImageLogo';
import imgBac from '../../images/diploma.png';
import imgCharette from '../../images/charette.png';
import imgPoker from '../../images/cards.png';
import imgCook from '../../images/cooking.png';
import imgBook from '../../images/books.png';
import imgDice from '../../images/dice.png';
import imgOpen from '../../images/openClassrooms.png';

import Foot from './Foot';
import './Presentation.css';

class Presentation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isHover: [0,0,0,0,0,0,0],
      isNeverHover: true,
    };
  }
  _onMouseEnter = event => {
    let imgAlt = event.currentTarget.firstChild.alt;
    if (imgAlt === 'bac') {
      this.setState({isHover: [1,0,0,0,0,0,0]});
    } else if (imgAlt === 'charette') {
      this.setState({isHover: [0,1,0,0,0,0,0]});
    } else if (imgAlt === 'poker') {
      this.setState({isHover: [0,0,1,0,0,0,0]});
    } else if (imgAlt === 'cook') {
      this.setState({isHover: [0,0,0,1,0,0,0]});
    } else if (imgAlt === 'book') {
      this.setState({isHover: [0,0,0,0,1,0,0]});
    } else if (imgAlt === 'dice') {
      this.setState({isHover: [0,0,0,0,0,1,0]});
    } else if (imgAlt === 'open') {
      this.setState({isHover: [0,0,0,0,0,0,1]});
    }
    this.setState({isNeverHover: false});
  }
  _onMouseLeave = () => {
      this.setState({isHover: [0,0,0,0,0,0,0]});
  }
  render() {
    const {isHover, isNeverHover} = this.state;
    const [classNameShadow, classNameImg, classNames] = [[],[],[]];
    for (let i = 0; i < 7; i++) {
       classNameShadow[i] = isHover.indexOf(1) === i ? ('object')+(i+1).toString()+'__shadow--anim' : ('object'+(i+1).toString()+'__shadow');
       classNames[i] = isHover.indexOf(1) === i ? ('object')+(i+1).toString()+'--anim' : ('object'+(i+1).toString());
       classNameImg[i] = isHover.indexOf(1) === i ? ('object')+(i+1).toString()+'__img--anim' : ('object'+(i+1).toString()+'__img');
    }
    return(
      <div className="presentation" id="présentation">
        {[0,1,2,3,4,5,6,7,8,9,10,11,12].map(item => {
          if (item%2 !== 0) {
            return <Foot
                    number={data[item].number}
                    className={data[item].className}
                    time={data[item].time}
                    key={item}
                   />
          } else{
            return <ImageLogo 
                    isNeverHover={isNeverHover}
                    classNames={data[item].classNames}
                    classNameShadow={classNameShadow[(item/2)]}
                    classNameImg={classNameImg[(item/2)]}
                    src={data[item].src}
                    alt={data[item].alt}
                    delay={data[item].delay}
                    offset={data[item].offset}
                    onMouseEnter={this._onMouseEnter}
                    onMouseLeave={this._onMouseLeave}
                    key={item}
                  
                  />
            }
          })
        }
        {isHover.indexOf(1) !== -1 && (
          <TextAbout className={dataTextAbout[isHover.indexOf(1)+1].className} number={isHover.indexOf(1)+1} />
        )}
      </div>
    );
  }
}

const data = {
  0:{
    classNames: 'object1',
    src: imgBac,
    alt:"bac",
    delay:'100',
    offset:'0',
  },
  1: {
    number: '5',
    className: 'container__footer1',
    time: '100',
  },
  2:{
    classNames: 'object2',
    src: imgCharette,
    alt:'charette',
    delay:'600',
    offset:'-300',
  },
  3: {
    number: '6',
    className: 'container__footer2',
    time: '600',
  },
  4:{
    classNames: 'object3',
    src: imgPoker,
    alt:'poker',
    delay:'1100',
    offset:'0',
  },
  5: {
    number: '3',
    className: 'container__footer3',
    time: '1100',
  },
  6:{
    classNames: 'object4',
    src: imgCook,
    alt:'cook',
    delay:'1400',
    offset:'-300',
  },
  7: {
    number: '6',
    className: 'container__footer4',
    time: '1400',
  },
  8:{
    classNames: 'object5',
    src: imgBook,
    alt:'book',
    delay:'2000',
    offset:'0',
  },
  9: {
    number: '6',
    className: 'container__footer5',
    time: '2000',
  },
  10:{
    classNames: 'object6',
    src: imgDice,
    alt:'dice',
    delay:'2400',
    offset:'-400',
  },
  11: {
    number: '5',
    className: 'container__footer6',
    time: '2400',
  },
  12:{
    classNames: 'object7',
    src: imgOpen,
    alt:'open',
    delay:'2900',
    offset:'0',
  },
};
const dataTextAbout = {
  1: {
    className: 'text__about1',
  },
  2: {
    className: 'text__about2',
  },
  3: {
    className: 'text__about3',
  },
  4: {
    className: 'text__about4',
  },
  5: {
    className: 'text__about5',
  },
  6: {
    className: 'text__about6',
  },
  7: {
    className: 'text__about7',
  },
};

export default Presentation;
