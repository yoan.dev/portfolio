import React, {Component} from 'react';
import Dices from '../Dices';
import pokerPresentation from '../../../images/pokerPresentation.png';
import imgPlayer from '../../../images/player.png'
import imgPokerAction from '../../../images/pokerAction.png';
import imgFlop from '../../../images/pokerFlop.png';
import imgTurn from '../../../images/pokerTurn.png';
import picoSolo from '../../../images/pico_solo.png';
import picoChallenge from '../../../images/pico_challenge.png';
import picoDuel from '../../../images/pico_duel.png';
import picoGeneral from '../../../images/pico_general.png';
import picoStore from '../../../images/pico_store.png';
import openclassroom from '../../../images/ocPresentation.png'
import imgBattle from '../../../images/battle.png';
import './ContainProject.css';


class ContainProject extends Component {
  render() {
    const {name, page, display, value} = this.props;
    let view, classNames;
    if (name === 'poker') classNames = data[name][page].classNames;
    if (name === 'pico') classNames = 'view__display1__contain__img';
    if (name === 'openclassroom') classNames = 'view__display1__contain__img--openclassroom';
    if (display === 1) {
      view = (
        <div className="view__display1" onClick={() => this._changeValue}>
          <div className="view__display1__container__img" >
            <img src={data[name][page].src} alt='poker' className={classNames} />
          </div>
          <div className={name === 'poker' ? "view__display1__container__text--poker" : "view__display1__container__text"} >
            <p className={(name === 'openclassroom' && page !== 1) ? "view__display1__contain__text__title--right" : "view__display1__contain__text__title"}>
              {data[name][page].title}
            </p>
              <div>{data[name][page].sentence.map(i => (i))}</div>
              {name === 'poker' ?
                data[name][page].point.map(i => (
                  <div className="view__display3__container__point"key={i} >
                    <div className="view__display1__point" ></div>
                    <p className="view__display4__contain__text__point" >{i}</p>
                  </div>
              )) : (
                data[name][page].point.map(i => (
                  <div className="view__display2__container__point" key={i} >
                    <div  className="view__display2__container2__point" >
                      <div className="view__display2__point--right" ></div>
                    </div>
                    {i}
                  </div>
                ))
              )
              
              }
              {name === 'pico' && page === 1 && <Dices value={value}/>}
          </div>
        </div>
      )
    } else if (display === 2) {
      view = (
        <div className="view__display2" >
          <div className="view__display2__container__text" >
            <p 
            className={name === 'openclassroom' ? "view__display2__contain__text__title--oc" : (
              page === 4 ? "view__display2__contain__text__title--reussite" : "view__display2__contain__text__title"
            )}
             >{data[name][page].title}</p>
            <div>{data[name][page].sentence}</div>
              {data[name][page].point.map(i => (
                <div className="view__display2__container__point" key={i}>
                  <div  className="view__display2__container2__point" >
                    <div className={name === 'pico' ? "view__display2__point" : "view__display2__point--oc"} ></div>
                  </div>
                  {i}
                </div>
              ))}
          </div>
            <div className="view__display2__container__img" >
              <img src={data[name][page].src} alt='poker' className={name === 'pico' ? "view__display2__contain__img" : "view__display2__contain__img--battle"} />
            </div>
        </div>
      )
    } else if (display === 3) {
      view = (
        <div className="view__display3">
          <div className="view__display3__container__img">
            <img src={data[name][page].src} alt='poker' className="view__display3__contain__img"/>
          </div>
          <div className="view__display3__container__text">
            <p className="view__display3__contain__text__title">{data[name][page].title}</p>
            <div>
              {data[name][page].sentence.map((i, index) => {
                if (index !== data[name][page].sentence.length-1) 
                return i;
                return null;
              })}
              {data[name][page].point.map(i => (
                <div className="view__display3__container__point" key={i}>
                  <div className="view__display1__point" ></div>
                  <p className="view__display4__contain__text__point" >{i}</p>
                </div>
              ))}
              <div>{data[name][page].sentence[data[name][page].sentence.length-1]}</div>
            </div>
          </div>
        </div>
      )
    } else if (display === 4) {
      view = (
        <div className="view__display4">
          <div className="view__display4__top">
            <div className="view__display4__top__container__img">
              <img src={imgPlayer} alt='player' className="view__display4__top__contain__img"/>
            </div>
            <div className="view__display4__top__container__text">
              <p className="view__display4__top__contain__text__sentence">Imaginons un coup de poker, on voit les paramètres dont on a besoin. Notre adversaire mise simplement, 
              il effectue donc un 2Bet.</p>
            </div>
          </div>
          <div className="view__display4__bottom">
            <div className="view__display4__bottom__container__img">
              <img src={imgPokerAction} alt='poker' className="view__display4__bottom__contain__img" />
            </div>
            <div className="view__display4__bottom__container__text">
            <p className="view__display4__bottom__contain__text__sentence">On choisit "Vilain a 2Bet" pour exprimer son action, ensuite on entre son eventail,
            on sait qu'il a 20% de 2Bet, en rentre les cartes du flop et sa couleur.</p>
            </div>
            
          </div>
        </div>
      )
    }
    return(
        <div className="contain__project" >
          {view}
        </div>
    );
  }
}

const data = {
  'poker': {
    1: {
      classNames: 'view__display1__contain__img--poker',
      title: 'Présentation',
      src: pokerPresentation,
      sentence: [<p className="view__display1__contain__text__sentence">Cet algorithme a été conçu dans le but d'approfondir ma connaissance du jeu.<br/><br/></p>,
       <p className="view__display1__contain__text__sentence">Ce programme analyse le déroulement d'une main au poker à travers l'action produite par le joueur adversaire
       et cinq paramètres:</p>],
      point: ['2Bet - Action de miser avant les autres.', 'Call 2Bet - Action de suivre une Mise.',"3Bet - Action de relancer une mise.", 
      "Call 3Bet - Action de suivre une relance de mise.", "VPIP - Eventail de mains joué exprimé en %."],
    },
    2: {
      src1: imgPlayer,
      src2: imgPokerAction,
      sentence: [],

    },
    3: {
      title: "Analyse du Flop",
      sentence: [
        <p className="view__display3__contain__text__sentence">Cette interface analyse le Flop en distinguant par ordre hierarchique la force de sa main.
        Pour avoir une comparaison, il est connu au poker qu'en moyenne un joueur "touche" le flop une 1 sur 3. 
        Par toucher on entend avoir au minimum une pair, ou bien un tirage. On note "se coucher" signifiant abandonner la main.</p>,
        <p className="view__display3__contain__text__sentence"><br/>Il touche 62.79% du temps le flop, soit 2 fois sur 3. Les deux premières lignes indiquent 
        que 44.19% des cas temps il a au minimum Première paire/Gros tirage.</p>,
      ],
      src: imgFlop,
      point: ["Incouchable au flop - Paire supérieure / Brelan / Double paire - Très rare et extrèmement fort au flop - 17.83%",
      "Ne se couche pas au flop - Meilleur paire du flop - Gros tirage - Rare et fort au flop - 26.36%",
      "Ne se couchera pas directement au flop - Deuxième paire / Faible tirage - Moyen, peu de potentiel - 18.60%",
      "Qui devrait se coucher - Troisième paire / Deux cartes hautes - Faible, sans potentiel d'améliorer - 13.18%",
      "Qui doit se coucher au flop - Quatrième paire / Une carte haute / Rien... - À jetter sans réfléchir - 24.03%"]
      
    },
    4: {
      title: 'Analyse de la Turn',
      sentence: [
        <p className="view__display3__contain__text__sentence">Ici on analyse la Turn. L'eventail de l'adversaire a changé, les mains qui étaient obsolètes
        on était retiré. La carte suivante est un 10, elle améliore plusieurs mains - TT (brelan) - AK (suite) - QT(double paires) - JT(double paires).<br/>
        Cette décomposition nous facilite à connaitre la main de l'adversaire en fonction de son action: </p>,
        <p className="view__display3__contain__text__sentence"><br/> Cette analyse permet de mieux comprendre le jeu ainsi qu'une meilleur lecture des actions 
        des différents joueurs. Ce programme datant de 2015, beaucoups de chose peuvent y être améliorer en terme technique mais aussi lui donner une 
        interface utilisateur.</p>,
      ],
      src: imgTurn,
      point: ['Mise très forte à Tapis - sa main se trouve majoritairement dans la catégorie "Incouchable à la turn"',
      'Mise moyenne à très forte - Sa main se trouve dans la catégorie "Ne se couchera pas à la turn"',
      'Mise faible à moyenne - Sa main se trouve dans "Ne se couche pas directement à la turn"',
      'Check - Sa main n\'as plus de valeur, catégorie "Doit se coucher à la turn"'],
    },
  },
  'pico': {
    1: {
      title: 'PICO',
      src: picoSolo,
      sentence: [<p className="view__display1__contain__text__sentence">
          PICO est un prototype frontend d'application conçu entre Avril et Juin 2019 avec React & Redux dans le but 
          d'appliquer mes connaissances théoriques.<br/><br/> Le but du jeu est de trouver un nombre en utilisant TOUS les dès mis à dispositions et en se servant de trois 
          opérations, l'addition, la soustraction, et la multiplication. <br/>
        </p>,
        
      ],
      point: [],
    },
    2: {
      title: 'Défis',
      src: picoChallenge,
      sentence: [
        <p className="view__display2__contain__text__sentence">
          Dans cette section trois défis sont proposé à travers les différents niveaux de difficulté (3, 4 , 5) dès :<br/><br/>
        </p>,
      ],
      point: [
        <p className="view__display2__contain__text__sentence">
          Le premier défis consiste à trouver un nombre avant un temps imparti, si le nombre a était trouvé, le compteur recommence son décompte,
          tant qu'on trouve le nombre demandé le compteur recommence, le but étant trouver le plus possible sans briser la chaîne.<br/><br/>
        </p>,
        <p className="view__display2__contain__text__sentence">
          Pour le deuxième défis, le joueur doit trouver le plus de nombre avant l'arrêt du chronomètre.<br/><br/>
        </p>,
        <p className="view__display2__contain__text__sentence">
          Le dernier défis permet de résoudre les nombres les plus difficile à trouver dans les catégories (4, 5, 6) dès.
      </p>,
      ],
    },
    3: {
      title: 'Duel',
      src: picoDuel,
      sentence: [
        <p className="view__display2__contain__text__sentence--reverse">
          Ce mode multijoueur permet d'affronter des joueurs du même niveau que soi à l'aide d'un système de point.<br/><br/>
        </p>,
      ],
      point: [
        <p className="view__display2__contain__text__sentence--reverse">
          La première est un duel dure 2min 30s, il y a deux façons pour gagner. Trouver plus de nombre que son adversaire à la fin du chronomètre ou en trouver
           trois d'affilé avant que notre adversaire en trouve un seul ce qui éliminera le joueur.<br/><br/>
        </p>,
        <p className="view__display2__contain__text__sentence--reverse">
        La deuxième est un mode "survie", plusieurs joueurs s'affronte, un temps imparti s'écoule, les joueurs qui n'ont pas trouvé
        le nombre sont éliminé.<br/>À chaque nouveau nombre le temps imparti diminu jusqu'à ce qu'il n'y est qu'un seul joueur.
      </p>,
      ],
    },
    4: {
      title: 'Réussite',
      src: picoGeneral,
      sentence: [
        <p className="view__display2__contain__text__sentence">
          Dans cette section est stocké les réussites du joueur permettant d'obtenir des points, des titres
          et de nouveaux design de dès. Au total 43 différentes cellules "réussites". Exemple de réussite par sous-section : <br/><br/>
        </p>,
      ],
      point: [
        <p className="view__display2__contain__text__sentence">
          Solo (Finir mode moyen -  Atteindre 3 étoiles en mode difficile - Finir les modes facile, moyen et difficile)<br/><br/>
        </p>,
        <p className="view__display2__contain__text__sentence">
          Défis (Faire une chaîne de 20 nombres avec 4 dès - Trouver 10 nombres avec 3 dès - résoudre 10 puzzles avec 3 dès)<br/><br/>
        </p>,
        <p className="view__display2__contain__text__sentence">
          Duel (Atteindre 1000 trophés - battre 100 adversaires - Éliminer 50 adversaires - Gagner 3 fois d'afilé en mode
          survie à 5 joueurs)<br/><br/>
        </p>,
        <p className="view__display2__contain__text__sentence">
          Général (Trouver tous les nombres de 1 à 100 - Faire tous les jets de dès possible avec 4 dès - Collecter 3 lots de dès - 
          Collecter 5 titres)<br/><br/>
        </p>,
        <p className="view__display2__contain__text__sentence">
          Stat - Ce trouve toutes  les statistiques du joueur et des courbes de progression par jour / 
          semaine / mois.
      </p>,
      ],
    },
    5: {
      title: 'Magasin',
      src: picoStore,
      sentence: [
        <p className="view__display2__contain__text__sentence--reverse">
          Le magasin nous offres une multitude de design à acheter avec des points de réussites. Il existe 3 catégories de design allant
          du moins sophistiqué au plus séduisant.<br/>13 designs sont à acheter et 17 designs particulier à débloquer avec les défis.<br/><br/>
        </p>,
      ],
      point: [
        <p className="view__display2__contain__text__sentence--reverse">
          Dès pour débutant - 8 designs différents coutant entre 75 et 125 points de réussites.<br/><br/>
        </p>,
        <p className="view__display2__contain__text__sentence--reverse">
          Dès pour expert - 4 designs différents coutant 200 ou 250 points de réussites.<br/><br/>
      </p>,
      <p className="view__display2__contain__text__sentence--reverse">
        A manipuler avec précaution! - 2 gros designs coutant 500 points de réussites.
      </p>,
      ],
    },
  },
  'openclassroom': {
    1: {
      title: 'Formation',
      src: openclassroom,
      sentence: [<p className="view__display1__contain__text__sentence--oc1">
          C'est grâce à OpenClassroom que j'ai débuté dans la programmation notamment pour apprendre le langage C pour développer
          l'algorithme de poker et par la suite différents langages comme HTML, CSS3, Javascript, Git.<br/><br/>
          Depuis Novembre 2019, j'ai commencé une formation avec OpenClassroom pour dévenir développeur. Je suis actuellement
          en recherche d'une entreprise pour réaliser ma formation en alternance.<br/><br/>
          J'ai décidé de proposer deux parcours qui me tiennent à coeur aux entreprises, une orienté Front-End et l'autre
          Back-End.<br/> <br/>Albigeois(e), Toulousain(e), je vous invite à me contacter si vous êtes intéréssé par mon profil !
        </p>,
        
      ],
      point: [],
    },
    2: {
      title: 'Développeur Frontend',
      src: imgBattle,
      sentence: [
        <p className="view__display1__contain__text__sentence--left">
          Intéréssé par le développement Frontend et l'UX, cette formation de niveau Bac +3/4 me permettrait d'approfondir ma connaissance de javascript 
          et de son écosystème.<br/> Quelque points clés de la formation : <br/><br/>
        </p>,
      ],
      
      point: [
        <p className="view__display1__contain__text__sentence--left">
          Analyser un cahier des charges et choisir une solution technique adaptée parmi les solutions existantes.<br/><br/>
        </p>,
        <p className="view__display1__contain__text__sentence--left">
         Concevoir l’architecture technique d’une application à l’aide de diagrammes UML.<br/><br/>
        </p>,
        <p className="view__display1__contain__text__sentence--left">
          Créer des projets web dynamiques grâce Javascript.<br/><br/>
        </p>,
        <p className="view__display1__contain__text__sentence--left">
          Communiquer avec une base de données.<br/><br/>
        </p>,
        <p className="view__display1__contain__text__sentence--left">
          Exploiter une plate-forme d’outils ou framework telle que jQuery pour concevoir un projet web professionnel.<br/><br/>
        </p>,
        <p className="view__display1__contain__text__sentence--left">
          Mettre en oeuvre des tests unitaires et d’intégration.<br/><br/>
        </p>,
        <p className="view__display1__contain__text__sentence--left">
          Optimiser une interface pour qu’elle soit plus réactive.<br/><br/>
        </p>,
        <p className="view__display1__contain__text__sentence--left">
          Produire une documentation technique et fonctionnelle de l’application.
        </p>,
      ],
      
    },
    3: {
      title: 'Développeur Python',
      src: picoDuel,
      sentence: [
        <p className="view__display1__contain__text__sentence--right">
          Débutant en python et captiver par le Backend, je m'intéresse à la gestion des bases de données ainsi que des serveurs. Cette formation est de niveau 
          bac +3/4.<br/> Quelques points étudié dans la formation:<br/><br/>
        </p>,
      ],
      point: [
        <p className="view__display1__contain__text__sentence--right">
          Créer des projets web dynamiques grâce à Python.<br/><br/>
        </p>,
        <p className="view__display1__contain__text__sentence--right">
         Concevoir l’architecture technique d’une application à l’aide de diagrammes UML.<br/><br/>
        </p>,
        <p className="view__display1__contain__text__sentence--right">
          Analyser un cahier des charges et choisir une solution technique adaptée parmi les solutions existantes.<br/><br/>
        </p>,
        <p className="view__display1__contain__text__sentence--right">
          Communiquer avec une base de données pour stocker et requêter des informations.<br/><br/>
        </p>,
        <p className="view__display1__contain__text__sentence--right">
          Développer de manière professionnelle grâce au framework Django.<br/><br/>
        </p>,
        <p className="view__display1__contain__text__sentence--right">
          Mettre en oeuvre des tests unitaires et fonctionnels.<br/><br/>
        </p>,
        <p className="view__display1__contain__text__sentence--right">
          Produire une documentation technique et fonctionnelle de l’application.<br/><br/>
        </p>,
        
      ],
    },
  }
};

export default ContainProject;
