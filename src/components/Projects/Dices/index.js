import React, {Component} from 'react';
import dices1 from '../../../images/des1.png';
import dices2 from '../../../images/des2.png';
import dices3 from '../../../images/des3.png';
import dices4 from '../../../images/des4.png';
import dices5 from '../../../images/des5.png';
import dices6 from '../../../images/des6.png';

import './Dices.css';

class Dices extends Component {
  render() {
      const {value, changeValue} = this.props;
    return(
      <div>
        <div className="dices__container">
          <div className="dices__container__text">
            <p className="dices__contain__text">Essayez-vous même:</p>
          </div>
          <div className="dices">
            <p className="dices__contain__text">Trouver {dataDice[value].toFind} avec</p>
            {dataDice[value].dices.map(dice => {
              switch (dice) {
                  case 1:
                      return <div onClick={() => changeValue} key={dice}><img src={dices1} alt='des1' className="dice" /></div>
                  case 2:
                      return <img src={dices2} alt='des2' className="dice"  onClick={() => changeValue}/>
                  case 3:
                      return <img src={dices3} alt='des3' className="dice"  onClick={() => changeValue}/>
                  case 4:
                      return <img src={dices4} alt='des4' className="dice"  onClick={() => changeValue}/>
                  case 5:
                      return <img src={dices5} alt='des5' className="dice"  onClick={() => changeValue}/>
                  case 6:
                      return <img src={dices6} alt='des6' className="dice"  onClick={() => changeValue}/>
                  default: break;    
              }
              return null;
          })}
          </div>
        </div>
         
          
      </div>
      
    );
  }
}
const dataDice = {
    1: {
      toFind: 39,
      dices: [4,5,3,1,5],
    },
    2: {
      toFind: 15,
      dices: [6,6,3,6,3],
    },
    3: {
      toFind: 73,
      dices: [6,1,4,5,3],
    },
    4: {
      toFind: 49,
      dices: [1,4,1,2,5],
    },
    5: {
      toFind: 42,
      dices: [4,3,5,6,5],
    },
    6: {
      toFind: 21,
      dices: [6,3,5,2,4],
    },
    7: {
      toFind: 24,
      dices: [4,3,4,6,4],
    },
    8: {
      toFind: 54,
      dices: [2,4,1,5,5],
    },
    9: {
      toFind: 43,
      dices: [2,3,3,2,5],
    },
    10: {
      toFind: 71,
      dices: [6,5,3,5,2],
    },
    11: {
      toFind: 77,
      dices: [3,4,5,5,6],
    },
    12: {
      toFind: 22,
      dices: [2,1,3,5,5],
    },
    13: {
      toFind: 56,
      dices: [3,6,5,5,2],
    },
    14: {
      toFind: 10,
      dices: [5,6,1,5,1],
    },
    15: {
      toFind: 46,
      dices: [1,4,2,6,6],
    },
    16: {
      toFind: 43,
      dices: [5,3,6,6,2],
    },
    17: {
      toFind: 26,
      dices: [2,6,1,1,2],
    },
    18: {
      toFind: 60,
      dices: [6,4,5,6,6],
    },
    19: {
      toFind: 6,
      dices: [4,3,6,5,4],
    },
    20: {
      toFind: 8,
      dices: [6,2,4,6,6],
    },
  };

export default Dices;
