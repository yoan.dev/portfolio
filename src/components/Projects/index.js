import React, {Component} from 'react';
import ContainerProject from './ContainerProject';

import './Project.css';

class Projects extends Component {
  render() {
    const {onClick, isCarousel, projectFocus, name, handleClick, slideIndex, changeAfterSlide, } = this.props;
    return(
      <div className="projects" id="projets">
        <p className={name !== 'Mes projets' ? "projects__title--project" : "projects__title"}>{name.substr(0,1).toUpperCase() + name.substr(1)}</p>
        <ContainerProject 
          onClick={onClick}
          projectFocus={projectFocus}
          isCarousel={isCarousel}
          name={name}
          handleClick={handleClick}
          slideIndex={slideIndex}
          changeAfterSlide={changeAfterSlide}
        />
      </div>
    );
  }
}

export default Projects;
