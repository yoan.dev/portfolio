import React, {Component} from 'react';
import './OpenClassroom.css';

class OpenClassroom extends Component {
  render() {
    const {onClick, value} = this.props;
    return(
      <div 
        className="openclassroom__background" 
        onClick={event => onClick(event, value)}
      >
      </div>
    );
  }
}

export default OpenClassroom;
