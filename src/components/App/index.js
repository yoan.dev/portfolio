import React, {Component} from 'react';
import Contact from '../Contact';
import Header from '../Header';
import LandPage from '../Landpage';
import Presentation from '../Presentation';
import Projects from '../Projects';
import Skills from '../Skills';
import Footer from '../Footer';

import './App.css';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 'Mes projets',
      isCarousel: false,
      projectFocus: null,
      slideIndex: 0,
    }
  }
  _onClickSectionProjet = () => {
    this.setState({
      isCarousel: false,
      value: 'Mes projets',
    })
  }
  _onClickProjet = (event, value) => {
    this.setState({
      value,
      isCarousel: true,
      projectFocus: value,
      slideIndex: 0,
    });
  }
  handleClick = (event, value) => {
    event.preventDefault();
    this.setState({slideIndex:value});
  }
  _changeAfterSlide = (slideIndex) => {
    this.setState({slideIndex})
  }
  render() {
    const {isCarousel, projectFocus, value, slideIndex} = this.state;
    return(
      <div className="app">
        <Header onClick={this._onClickSectionProjet}/>
        <LandPage />
        <Presentation />
        <Skills />
        <Projects 
          onClick={this._onClickProjet}
          isCarousel={isCarousel}
          projectFocus={projectFocus}
          name={value}
          handleClick={this.handleClick}
          slideIndex={slideIndex}
          changeAfterSlide={this._changeAfterSlide}
        />
        <Contact />
        <Footer />
      </div>
    );
  }
}

export default App;
